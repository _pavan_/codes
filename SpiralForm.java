import java.util.Scanner;

public class SpiralForm {

	static void spiralPrint(int m, int n, int a[][])
    {
        int i, k = 0, l = 0;
        while (k < m && l < n)
        {
            for (i = l; i < n; ++i)
            {
                System.out.print(a[k][i]+" ");
            }
            k++;
  
            for (i = k; i < m; ++i)
            {
                System.out.print(a[i][n-1]+" ");
            }
            n--;
  
            if ( k < m)
            {
                for (i = n-1; i >= l; --i)
                {
                    System.out.print(a[m-1][i]+" ");
                }
                m--;
            }
  
            if (l < n)
            {
                for (i = m-1; i >= k; --i)
                {
                    System.out.print(a[i][l]+" ");
                }
                l++;    
            }        
        }
    }
     
    public static void main (String[] args) 
    {
     
    	System.out.println("Enter number of rows in matrix :");
    	Scanner sc = new Scanner(System.in);
    	int n = sc.nextInt();
    	System.out.println("Enter "+n*n +" elements of matrix :");
    	int a[][] = new int[n][n];
    	for(int i=0;i<n;i++) {
    		for(int j=0;j<n;j++) {
    			a[i][j]=sc.nextInt();
    		}
    	}
    	System.out.println("Printing matrix in spiral form : ");
        spiralPrint(n,n,a);
    }
	
}
